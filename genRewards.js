const fs = require('fs');

let str = fs.readFileSync('discoveryFeed.json');
let discoveryFeed = JSON.parse(str).result.feed;

let rewardsDeck = discoveryFeed.filter((f) => {
	if (f.type == "REWARD_DECK") {
		return true;
	}
	else {
		return false;
	}
})[0];

let rewards = rewardsDeck.cards.map((r) => {
	return {
	                "campaignId" : null,
	                "id" : 123456,
	                "type" : "MERCHANT",
	                "merchantName" : r.merchantName,
	                "merchantID" : "8a7180903d380cd6013d3c9d7b0c0015",
	                "captiveELoyaltyEnabled" : false,
	                "created" : new Date().getTime(),
	                "updated" : new Date().getTime(),
	                "published" : null,
	                "removed" : null,
	                "status" : "ACTIVE",
	                "rank" : 0,
	                "title" : r.merchantName,
	                "pointsValue" : 2200,
	                "value" : 10.0,
	                "honeybeeValue" : 0,
	                "imageUri" : r.merchantLogoUrl,
	                "description" : `!!Redeemable only at ${r.merchantName}.\n!!This Reward is redeemable for up to the face value and can only be used once.\n!!Any unused value will be lost after use. No cash back on unused value.\n!!Do not call number listed on REWARD. For any customer service issues contact Buzz Points directly at 1-877-577-2899.\n\n\n*$10.00 Buzz Point Reward*\n\nRedeemable only at ${r.merchantName}.\n\nThis Reward is redeemable for up to the face value and can only be used once.  Any unused value will be lost after use. No cash back on unused value. This Reward is not reloadable.\n\nThis Buzz Points Reward is not redeemable for cash (unless required by law). Promotional value cannot be combined with other offers.  \n\nUnauthorized reproduction, resale, modification, or trade prohibited. \n\n*Use or acceptance of this REWARD constitutes acceptance of these terms.*`,
	                "medium" : "ELECTRONIC",
	                "scope" : "LOCAL",
	                "fulfillment" : "IVR",
	                "delivery" : null,
	                "fastIndexId" : null,
	                "esSynced" : null,
	                "views" : 0,
	                "purchasesUndiscounted" : 0,
	                "redemptionsUndiscounted" : 0,
	                "outstandingUndiscounted" : 0,
	                "redeemedDiscounted" : 0,
	                "outstandingDiscounted" : 0,
	                "changeLogs" : [],
	                "loggedinUserId" : null,
	                "locations" : [{
	                        "id" : 0,
	                        "updated" : null,
	                        "created" : 1478275744423,
	                        "merchantID" : null,
	                        "merchantTitle" : null,
	                        "merchantType" : null,
	                        "merchantStatus" : null,
	                        "merchantLogoURL" : null,
	                        "categories" : null,
	                        "bonusMultiplier" : 0.0,
	                        "loyaltyCardActive" : false,
	                        "loyaltyCardMinTransactionAmountInCents" : 0,
	                        "gpsLat" : 30.2699369,
	                        "gpsLong" : -97.74167060000002,
	                        "address" : null,
	                        "city" : null,
	                        "state" : null,
	                        "zipcode" : null,
	                        "locationName" : null,
	                        "locationContactName" : null,
	                        "locationPhoneNumber" : null,
	                        "locationURL" : null,
	                        "transMappings" : null,
	                        "fastIndexId" : null,
	                        "distance" : null
	                    }
	                ],
	                "charityId" : null,
	                "charities" : null,
	                "distance" : null,
	                "bucket" : 1,
	                "isNgc" : null,
	                "ngc_ID" : null,
	                "preset" : false,
	                "additionalRequirements" : null,
	                "discount_Id" : 7237701,
	                "discountExpires" : 1543441692,
	                "discountPointsValue" : 440,
	                "discountValue" : 440,
	                "rewardPointsValueDiscounted" : 1760,
	                "rewardValueDiscounted" : 1760,
	                "discountPercentOff" : 20,
	                "printRequired" : false
	}	
});

console.log(JSON.stringify({result: {rewardsDiscountBucket: rewards}}));
