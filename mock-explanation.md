# Description

This file describes embedded mock jsons files and explains what are they responsible for  

# Discovery feed 

- `mock_discovery_feed.json` - Mocked response to the *api/discovery/feed* API call. Update this file to modify the content of the **Discover** page.

# Businesses Directory

- `mock_merchants_directory.json` - Mocked response to the *mobile/api/merchant/directory/* API call. Represents list of businesses displayed on **Businesses** page. 
- `mock_merchants_locations.json` - Mocked response to the *merchant-public/merchants/locations* API call. Represents pins that will be displayed on the map. Coordinates will be automatically generated every time user opens the **Businesses** page. Update title, icon, address etc, to make them consistent to content of the `mock_merchants_directory.json`
- `mock_merchant_details.json` - Mocked response to the *mobile/api/merchant/directory/{merchantId}/{fiid}* API call. It is used to generate content for **Merchant Details** page. Title, icon, description will be automatically updated with correspnding data from the merchant item selected on the previous page (Business Directory, Map, DIscover etc.). Update it to modify list of merchant specific list of offers and rewards.

# Rewards Directory

Files in this section are mocked responses to the  */mobile/api/rewardWithDiscount* API call with different category parameter values (LOCAL, NATIONAL, CHARITY)

- `mock_rewards_directory_local.json` - List of rewards displayed on *Rewards* page if the *Local* tab is selected
- `mock_rewards_directory_national.json` - List of rewards displayed on *Rewards* page if the *National* tab is selected
- `mock_rewards_directory_charity.json` - List of rewards displayed on *Rewards* page if the *Charity* tab is selected

# My Rewards & Redemption flow

- `mock_my_rewards.json` - Mocked response to the */fi/{fiid}/redemptions* API call. Represents **initial** list of redemptions on **My Rewards** page. All further redemptions made will be inserted to the top of this list. 
- `mock_redeem_response.json` - Mocked response to the *mobile/api/rewardWithDiscount/redeem* API call. It is partially used to generate content for **Redemption** page.
- `mock_redemptions_details.json` - Mocked response to the *redemptions/detail* API call. It is used to generate content for **Redemption** page. Title, icon, description will be automatically updated with correspnding data from the reward item selected on the previous page.

# Offers Directory

- `mock_offers_directory.json` - Mocked response to the *mobile/api/coupon/browse*. List of offers displayed on *Offers* page.

# Activities Directory 

- `mock_activities_list.json` - Mocked response to the *fi/{fiid}/activities*. Represents **initial** list of the activity items displayed on *Activity* page. All further redemptions made will be inserted to the top of this list as Redemption Activity items.

# Loyalty

- `mock_loyalty_cards.json` - Mocked response to the *loyaltycards/user*. List of loyalty cards displayed on *Loyalty Cards* page.
