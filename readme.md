# Description
This repo contains JSON merchant, rewards (including national and charity) and offers. The data is in the same format as the mobile API responses with one important difference:

Differences in image paths:
- `"imageUri": "http://i.imgur.com/1HYGihv.png?1"`: Image URIs are absolute.

# Simulating application flows

Your task is to simulate the application flows implied by the data hard coded in `discoveryFeed.json`.

# Application flows implied by the hard-coded discovery feed data

- The merchants in the hard-coded discovery feed must also appear in the merchant directory feed.
- If, in the hard-coded discovery feed, merchant M has reward R, then I should see reward R in the rewards list. The merchant and the reward share the same image. I should also be able to click on the reward detail view.
- The same applies to the offers. If merchant M has offer O in the discovery feed, then I should see the offer O in the offers feed. The merchant and the offer share the same image. I should be able to save the offer.

# Other application flows
- Redeeming a reward implies the "My Rewards" feed is updated.
- Saving an offer implies the "My Offers" feed is updated.
- Redeeming a reward implies the activity feed is updated with a new redemption.
- Local, national and charity rewards are all populated with data derived from the hard-coded discovery feed, in addition to the data in `rewardWithDiscountCharity.json` and 'rewardWithDiscountNational.json'
- All local, national and charity rewards in the rewards list also provide a reward detail view when clicked on.

# Define a data structure
It would be helpful to have one data structure that defines, for the entire application, the directory of merchants and their associated rewards, offers and loyalty cards. This will enable Buzz Points to easily customize the demo data based on the demo requirements. For example we may want to update the demo data to showcase more restaurants, or use higher quality merchant logos.

# Scripts for generating certain data
If you have Node Version Manager installed (nvm) you can run nodejs scripts to help generate mock data.

```
nvm use

node genOffers.js  # Generates a response from GET /mobile/api/coupon/browse using discoveryFeed.json

node genRewards.js # Generates a response from GET /mobile/api/rewardWithDiscount using discoveryFeed.json
```