const fs = require('fs');

let str = fs.readFileSync('discoveryFeed.json');
let discoveryFeed = JSON.parse(str).result.feed;

let coupons = discoveryFeed.filter((f) => {
	if (f.type == "COUPON_DECK") {
		return true;
	}
	else {
		return false;
	}
})[0];

let offers = coupons.cards.map((c) => {
	return {
            "id" : 123456,
            "expiryDate" : 1543441692,
            "fiid" : "democu",
            "merchantId" : "4028b881579a735a01579a74dd1c0021",
            "merchantTitle" : c.merchantName,
            "merchantLocations" : null,
            "title" : "Earn more points when you shop at this location",
            "imageHash" : c.merchantLogoUrl,
            "clickUrl" : "http://localhost:9090/offer?id=396",
            "created" : 1475765132662,
            "updated" : 1475765132708,
            "distance" : 0.25603514613024986,
            "saved" : false
    };
});

console.log(JSON.stringify({result: offers}));
