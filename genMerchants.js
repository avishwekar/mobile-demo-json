const fs = require('fs');

let str = fs.readFileSync('discoveryFeed.json');
let discoveryFeed = JSON.parse(str).result.feed;

let merchantDeck = discoveryFeed.filter((f) => {
	if (f.type == "MERCHANT_DECK") {
		return true;
	}
	else {
		return false;
	}
})[0];

let merchants = merchantDeck.cards.map((c) => {
      return {
                "id" : "ff80818145e4c2960145f659a1b83fbe",
                "updated" : 1441377454638,
                "created" : 1399997440438,
                "activeTimestamp" : 1399997440438,
                "tierID" : 1,
                "currentTier" : null,
                "title" : c.merchantName,
                "description" : c.description,
                "ownerName" : null,
                "ownerEmail" : null,
                "ownerPhone" : null,
                "merchantLogoURL" : c.merchantLogoUrl,
                "merchantPictureURLs" : [],
                "merchantChangeRequests" : [],
                "merchantCoupons" : [],
                "merchantBackgroundURL" : null,
                "merchantHook" : c.merchantHook,
                "categories" : [{
                        "id" : 409502,
                        "updated" : null,
                        "created" : 1478876061899,
                        "title" : "Food and Beverage",
                        "oldTitle" : null,
                        "categoryIconURL" : null,
                        "categoryIconURLx2" : null,
                        "categoryBackgroundImageURL" : null,
                        "otherCategoryKeywords" : null,
                        "parentID" : null,
                        "recommendedMultiplier" : null,
                        "selected" : false,
                        "fullyQualifiedTitle" : "Food and Beverage"
                    }
                ],
                "tags" : [],
                "locations" : [{
                        "id" : 1433660,
                        "updated" : 1399997440455,
                        "created" : 1399997440433,
                        "merchantID" : null,
                        "merchantTitle" : null,
                        "merchantType" : null,
                        "merchantStatus" : null,
                        "merchantLogoURL" : null,
                        "categories" : null,
                        "bonusMultiplier" : 0.0,
                        "loyaltyCardActive" : false,
                        "loyaltyCardMinTransactionAmountInCents" : 0,
                        "gpsLat" : 30.296682,
                        "gpsLong" : -97.767715,
                        "address" : "3110 Windsor Rd",
                        "city" : "Austin",
                        "state" : "TX",
                        "zipcode" : "78703",
                        "locationName" : null,
                        "locationContactName" : null,
                        "locationPhoneNumber" : null,
                        "locationURL" : null,
                        "transMappings" : null,
                        "fastIndexId" : null,
                        "distance" : null
                    }
                ],
                "fiid" : "vista",
                "contractid" : null,
                "status" : "ACTIVE",
                "type" : "LOCAL",
                "homepageURL" : null,
                "homepageLabel" : null,
                "homepageURL2" : null,
                "homepageLabel2" : null,
                "contactEmail" : null,
                "billingContact" : null,
                "companyName" : null,
                "billingAddress" : null,
                "billingCity" : null,
                "billingState" : null,
                "billingZip" : null,
                "billingEmail" : null,
                "directoryFeeSchedule" : null,
                "monthlyFee" : null,
                "directoryBillingStart" : null,
                "setupFeeBillingStart" : null,
                "pointsBillingStart" : null,
                "discountPercent" : null,
                "billingFrequency" : null,
                "annualFeeValue" : null,
                "annualFeeStart" : null,
                "annualFeeAutoRenew" : null,
                "loyaltyCard" : {
                    "merchantID" : null,
                    "numberofTransactions" : 0,
                    "pointsAwarded" : 0,
                    "loyaltyCardAwardERewardID" : null,
                    "isCapitiveELoyalty" : false,
                    "captiveELoyaltyERewardValue" : 0.0,
                    "minTransactionAmountInCents" : 0,
                    "loyaltyCardRewardExpiryDate" : null,
                    "title" : null,
                    "description" : null,
                    "startDate" : 1478876061899,
                    "endDate" : null,
                    "loyaltycardStatus" : "INACTIVE",
                    "currentlyActive" : false
                },
                "bonusPoints" : {
                    "multiplier" : 1.0,
                    "status" : "INACTIVE",
                    "activeTimestamp" : null
                },
                "testMerchant" : false,
                "distance" : "1mi",
                "score" : 0.9732201,
                "bucket" : 0,
                "bankName" : null,
                "bankRoutingNumber" : null,
                "bankAccountNumber" : null,
                "hasRewards" : false,
                "printRequired" : false,
                "merchantRewards" : [],
                "merchantRewardsWithDiscounts" : [],
                "coupons" : [],
                "hours" : [],
                "merchantProductSubscriptions" : [],
                "pointCampaigns" : [],
                "basePointCampaign" : null
            }
});

console.log(JSON.stringify(
      {
            totalPages: 1,
            page: 1,
            totalCount: merchants.length,
            result: merchants
      }
));
